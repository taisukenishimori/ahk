;;
;; WindowsでEmacs風キーバインド
;;
#InstallKeybdHook
#UseHook
;#Include IME.ahk

; C-x が押されると1になる
is_pre_x = 0
; C-Space が押されると1になる
is_pre_spc = 0
; C-[ が押されると1になる
is_pre_esc = 0

; Emacs風キーバインドを無効にしたいウィンドウ一覧
; 必要の無い部分はコメントアウトして下さい
is_target()
{
	IfWinActive,ahk_class Emacs
		Return 1
	IfWinActive,ahk_class IrfanView
		Return 1
	IfWinActive,ahk_class mintty
		Return 1
	IfWinActive,ahk_class PuTTY
		Return 1
	IfWinActive,ahk_class "MMP Skin Host"
		Return 1
	Return 0
}

delete_char()
{
	Send {Del}
	global is_pre_spc = 0
	Return
}
delete_backward_char()
{
	Send {BS}
	global is_pre_spc = 0
	Return
}
kill_line()
{
	Send {ShiftDown}{END}{SHIFTUP}
	Sleep 10 ;[ms]
	Send ^x
	global is_pre_spc = 0
	Return
}
open_line()
{
	Send {END}{Enter}{Up}
	global is_pre_spc = 0
	Return
}
quit()
{
	Send {ESC}
	global is_pre_spc = 0
	global is_pre_esc = 0
	global is_pre_x   = 0
	Return
}
newline()
{
	Send {Enter}
	global is_pre_spc = 0
	Return
}
indent_for_tab_command()
{
	Send {Tab}
	global is_pre_spc = 0
	Return
}
newline_and_indent()
{
	Send {Enter}{Tab}
	global is_pre_spc = 0
	Return
}
isearch_forward()
{
	Send ^f
	global is_pre_spc = 0
	Return
}
isearch_backward()
{
	Send ^f
	global is_pre_spc = 0
	Return
}
kill_region()
{
	Send ^x
	global is_pre_spc = 0
	Return
}
kill_ring_save()
{
	Send ^c
	Send ^c
	global is_pre_spc = 0
	global is_pre_esc = 0
	Return
}
yank()
{
	Send ^v
	global is_pre_spc = 0
	Return
}
undo()
{
	Send ^z
	global is_pre_spc = 0
	Return
}
find_file()
{
	Send ^o
	global is_pre_x = 0
	Return
}
save_buffer()
{
	Send, ^s
	global is_pre_x = 0
	Return
}
select_all()
{
	Send, ^a
	global is_pre_x = 0
	Return
}
kill_emacs()
{
	Send !{F4}
	global is_pre_x = 0
	Return
}

move_beginning_of_line()
{
	global
	if is_pre_spc
		Send +{HOME}
	Else
		Send {HOME}
	Return
}
move_end_of_line()
{
	global
	if is_pre_spc
		Send +{END}
	Else
		Send {END}
	Return
}
move_beginning_of_buffer()
{
	global
	if is_pre_spc
		Send ^+{HOME}
	Else
		Send ^{HOME}

	is_pre_esc = 0
	Return
}
move_end_of_buffer()
{
	global
	if is_pre_spc
		Send ^+{END}
	Else
		Send ^{END}

	is_pre_esc = 0
	Return
}
previous_line()
{
	global
	if is_pre_spc
		Send +{Up}
	Else
		Send {Up}
	Return
}
next_line()
{
	global
	if is_pre_spc
		Send +{Down}
	Else
		Send {Down}
	Return
}
forward_char()
{
	global
	if is_pre_spc
		Send +{Right}
	Else
		Send {Right}
	Return
}
backward_char()
{
	global
	if is_pre_spc
		Send +{Left}
	Else
		Send {Left}
	Return
}
scroll_up()
{
	global
	if is_pre_spc
		Send +{PgUp}
	Else
		Send {PgUp}
	Return
}
scroll_down()
{
	global
	if is_pre_spc
		Send +{PgDn}
	Else
		Send {PgDn}
	Return
}

edit_using_emacs()
{
	Send !t
	Send {Up}{Return}
	Return

}


;;;;;-------- 2012/8/20 --------from
;^x::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		is_pre_x = 1
;	Return
^[::
	If is_target()
		Send %A_ThisHotkey%
	Else
		is_pre_esc = 1
	Return
^f::
	If is_target()
		Send %A_ThisHotkey%
	Else
	{
		If is_pre_x
		find_file()
		Else
		forward_char()
	}
	Return
;^c::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;	{
;		If is_pre_x
;		kill_emacs()
;	}
;	Return
;h::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else If is_pre_x
;		select_all()
;	Else
;		Send %A_ThisHotkey%
;	Return
;
;w::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else If is_pre_esc
;		kill_ring_save()
;	Else
;		Send %A_ThisHotkey%			
;	Return
<::
	If is_target()
	Send %A_ThisHotkey%
	Else If is_pre_esc
		move_beginning_of_buffer()
	Else
		Send %A_ThisHotkey%			
	Return
>::
	If is_target()
		Send %A_ThisHotkey%
	Else If is_pre_esc
		move_end_of_buffer()
	Else
		Send %A_ThisHotkey%			
	Return
;;;;;-------- 2012/8/20 --------to


^d::
	If is_target()
		Send %A_ThisHotkey%
	Else
		delete_char()
	Return
^h::
	If is_target()
		Send %A_ThisHotkey%
	Else
		delete_backward_char()
	Return

;;;;;-------- 2012/8/20 --------from
;^k::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		kill_line()
;	Return
;^o::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else If IME_IsON(WinExist("A"))
;		Send %A_ThisHotkey%
;	Else
;		open_line()
;	Return
;^g::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		quit()
;	Return
;^j::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		newline_and_indent()
;	Return
;;;;;-------- 2012/8/20 --------to


^m::
	If is_target()
		Send %A_ThisHotkey%
	Else
		newline()
	Return

;;;;;-------- 2012/8/20 --------from
;^i::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else If IME_IsON(WinExist("A"))
;		Send %A_ThisHotkey%
;	Else
;		indent_for_tab_command()
;	Return
;^s::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;	{
;		If is_pre_x
;			save_buffer()
;		Else
;			isearch_forward()
;	}
;	Return
;^r::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		isearch_backward()
;	Return
;^w::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		kill_region()
;	Return
;!w::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		kill_ring_save()
;	Return
;^y::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		yank()
;	Return
;^/::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		undo()
;	Return
;;;;;-------- 2012/8/20 --------to


^vk20sc039::
	If is_target()
		Send {CtrlDown}{Space}{CtrlUp}
	Else
	{
		If is_pre_spc
			is_pre_spc = 0
		Else
			is_pre_spc = 1
	}
	Return
^@::
	If is_target()
		Send %A_ThisHotkey%
	Else
	{
		If is_pre_spc
			is_pre_spc = 0
		Else
			is_pre_spc = 1
	}
	Return
;^a::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		move_beginning_of_line()
;	Return
;^e::
;	If is_target()
;		Send %A_ThisHotkey%
;	Else
;		move_end_of_line()
;	Return
^p::
	IfWinActive,ahk_class IrfanView
		Send %A_ThisHotkey%
	If is_target()
		Send %A_ThisHotkey%
	Else
		previous_line()
	Return
^n::
	If is_target()
		Send %A_ThisHotkey%
	Else
		next_line()
	Return
^b::
	If is_target()
		Send %A_ThisHotkey%
	Else
		backward_char()
	Return

#k::
	MsgBox, 4,, スクリプトを終了しますか?,
	IfMsgBox, Yes
		ExitApp
	Return

vk1Dsc07B & d::
	Send !{F4}
	Return

vk1Dsc07B & s::WinMinimize,A
vk1Dsc07B & f::WinMaximize,A
vk1Dsc07B & g::WinRestore,A

NumLock::
	Send, 9047878
	Send, {TAB}
	Send, temma185
	Send, {ENTER}
	Return

Ins::
	Send, 9047878@japan.gds.panasonic.com
	Send, {TAB}
	Send, temma185
	Send, {ENTER}
	Return

Pause::
	Send, taisuke{TAB}47878{ENTER}
	Return


;;------------------------------------------------------------
IME_IsON(hWindow)
{
	; WM_IME_CONTROL    = 0x0283
	; IMC_GETOPENSTATUS = 0x0005
	bufCurrentDetectMode := A_DetectHiddenWindows
	DetectHiddenWindows, On
	buf := DllCall("user32.dll\SendMessageA", "UInt", DllCall("imm32.dll\ImmGetDefaultIMEWnd", "Uint",hWindow), "UInt", 0x0283, "Int", 0x0005, "Int", 0)
	DetectHiddenWindows, %bufCurrentDetectMode%
	Return buf
}

/*----------------------------------------------------------------------------
  IME 制御用 Functuion (コピペ用)
   動作確認環境  2005/09/12
     WinNT4 SP6 / WinXP SP2 (98系でもおそらくはOK)

    IME_Func.ahk からよく使う IME状態取得/セット部のみ抽出
    各関数間に依存関係はありません。
    関数間の依存関係は無いので
    必要な関数単体で

    2008.07.11 v1.0.47以降の 関数ライブラリスクリプト対応用にファイル名を修正
-----------------------------------------------------------------------------
*/


IME_GET(WinTitle="")
;-----------------------------------------------------------
; IMEの状態の取得
;    対象： AHK v1.0.34以降
;   WinTitle : 対象Window (省略時:アクティブウィンドウ)
;   戻り値  1:ON 0:OFF
;-----------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_GETOPENSTATUS
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x005,0,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

IME_SET(setSts, WinTitle="")
;-----------------------------------------------------------
; IMEの状態をセット
;    対象： AHK v1.0.34以降
;   SetSts  : 1:ON 0:OFF
;   WinTitle: 対象Window (省略時:アクティブウィンドウ)
;   戻り値  1:ON 0:OFF
;-----------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_SETOPENSTATUS
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x006,setSts,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

;===========================================================================
; IME 入力モード 取得 / セット
;
;    0000xxxx    かな入力
;    0001xxxx    ローマ字入力
;    xxxx0xxx    半角
;    xxxx1xxx    全角
;    xxxxx000    英数
;    xxxxx001    ひらがな
;    xxxxx011    ｶﾅ/カナ
;
;     0 (0x00  0000 0000) かな    半英数
;     3 (0x03  0000 0011)         半ｶﾅ
;     8 (0x08  0000 1000)         全英数
;     9 (0x09  0000 1001)         ひらがな
;    11 (0x0B  0000 1011)         全カタカナ
;    16 (0x10  0001 0000) ローマ字半英数
;    19 (0x13  0001 0011)         半ｶﾅ
;    24 (0x18  0001 1000)         全英数
;    25 (0x19  0001 1001)         ひらがな
;    27 (0x1B  0001 1011)         全カタカナ

IME_GetConvMode(WinTitle="")
;-------------------------------------------------------
; IME 入力モード取得
;   in  WinTitle    (省略時アクティブウィンドウ)
;   戻り値:  入力モード
;--------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_GETCONVERSIONMODE
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x001,0,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

IME_SetConvMode(ConvMode,WinTitle="")
;-------------------------------------------------------
; IME 入力モードセット
;   in  ConvMode    入力モード
;       WinTitle    (省略時アクティブウィンドウ)
;--------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_SETCONVERSIONMODE
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x002,ConvMode,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

;===========================================================================
; IME 変換モード
;    0:無変換
;    1:人名/地名
;    8:一般
;   16:話し言葉優先

IME_GetSentenceMode(WinTitle="")
;-------------------------------------------------------
; IME 変換モード取得
;   in  WinTitle    (省略時アクティブウィンドウ)
;   戻り値:  0:無変換 1:人名/地名 8:一般 16:話し言葉優先
;--------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_GETSENTENCEMODE
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x003,0,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

IME_SetSentenceMode(SentenceMode,WinTitle="")
;-------------------------------------------------------
; IME 変換モードセット
;   in  SentenceMode 0:無変換 1:人名/地名 8:一般 16:話し言葉優先
;       WinTitle     (省略時アクティブウィンドウ)
;--------------------------------------------------------
{
    ifEqual WinTitle,,  SetEnv,WinTitle,A
    WinGet,hWnd,ID,%WinTitle%
    DefaultIMEWnd := DllCall("imm32\ImmGetDefaultIMEWnd", Uint,hWnd, Uint)

    ;Message : WM_IME_CONTROL  wParam:IMC_SETSENTENCEMODE
    DetectSave := A_DetectHiddenWindows
    DetectHiddenWindows,ON
    SendMessage 0x283, 0x004,SentenceMode,,ahk_id %DefaultIMEWnd%
    DetectHiddenWindows,%DetectSave%
    Return ErrorLevel
}

;vk1Csc079 & L::AltTabMenu
;L::AltTab

;;;;------------alternate Page Up(from) -----------
vk1Csc079 & k::
	If is_target()
		Send %A_ThisHotkey%
	Else
		scroll_up()
	Return
;;;;------------alternate Page Up(to) -------------
;;;;------------alternate Page Down(from) ---------
vk1Csc079 & j::
	If is_target()
		Send %A_ThisHotkey%
	Else
		scroll_down()
	Return
;;;;------------alternate Page Down(to) -----------
;;;;------------alternate BackSpace(from) ---------
vk1Csc079 & h::
	If is_target()
		Send %A_ThisHotkey%
	Else
		Send {BS}
	Return
;;;;------------alternate Back Space(to) -----------

;;; ------------------------------------------------
;;; ウィンドウの切り替え(F6)を、「カナ」キーに割り当てる
vkF2sc070::F6
;;; ------------------------------------------------
;;; 「Ctrl + Up::ホィールマウスの↑」「Ctrl + Down::ホィールマウスの↓」
^vkE2sc073::WheelUp
^RCtrl::WheelDown
;;; ------------------------------------------------
F10::
Send, {AppsKey}oov
Return
;;; ------------------------------------------------
; F8::
; 	Run,iexplore http://iweb.mei.co.jp/epochref/webapp/WNMZS031/CallEpoch?view=CW000
; 	WinWait, https://iweb.mei.co.jp/ - Login - Windows Internet Explorer ahk_class IEFrame
; 	Sleep, 300
; 	Send, 9047878
; 	Send, {TAB}
; 	Send, temma185
; 	Send, {ENTER}
; 	WinWait, http://iweb.mei.co.jp/ - EPOCHシステム - 西森　泰輔 - Windows Internet Explorer ahk_class IEFrame
; 	Sleep, 1000
; ;	StatusBarWait, "javascript"
; ;	Sleep, 2000
; 	MouseClick LEFT, 331, 233
; 	Sleep, 1000
; 	MouseClick LEFT, 137, 228
; ;	Sleep, 500
; 	Sleep, 1000
; 	MouseClick LEFT, 151, 192
; 	Return
; ;;; ------------------------------------------------
F9::
	Send, 9047878
	Send, {TAB}
	Send, 1234qwer
	Send, {ENTER}
	WinWait, http://iweb.mei.co.jp/ - EPOCHシステム - 西森　泰輔 - Windows Internet Explorer ahk_class IEFrame
	Sleep, 1000
	MouseClick LEFT, 331, 233
	MouseClick LEFT, 137, 228
	Sleep, 500
	MouseClick LEFT, 151, 192
	Return
;;; ------------------------------------------------


;;;#Persistent
;;;	OnExit,WatchShutDown
;;;return
;;;
;;;WatchShutDown:
;;;if A_ExitReason=ShutDown
;;;    Run, iexplore http://iweb.mei.co.jp/epochref/webapp/WNMZS031/CallEpoch?view=CW000
;;;    WinWait, https://iweb.mei.co.jp/ - Login - Windows Internet Explorer ahk_class IEFrame
;;;ExitApp


#Persistent  ; For demonstration purposes.
OnExit, ExitSub
Return

ExitSub:
if A_ExitReason <> Logoff
	if A_ExitReason <> Shutdown
	{
		MsgBox, 4, , Are you sure you want to exit?
		IfMsgBox, No, return
	}
ExitApp

;;;;; ------------------------------------------------
;;F5::
;;	MouseClick LEFT, 52, 58
;;	Sleep, 500
;;	MouseClick LEFT, 52, 98
;;	Sleep, 500
;;	MouseClick LEFT, 52, 135
;;	Sleep, 500
;;	MouseClick LEFT, 52, 171
;;	Sleep, 500
;;	MouseClick LEFT, 52, 211
;;	Sleep, 500
;;	MouseClick LEFT, 52, 246
;;	Return
;;;;; ------------------------------------------------
;;F6::
;;	MouseClick LEFT, 52, 246
;;	Sleep, 500
;;	MouseClick LEFT, 52, 211
;;	Sleep, 500
;;	MouseClick LEFT, 52, 171
;;	Sleep, 500
;;	MouseClick LEFT, 52, 135
;;	Sleep, 500
;;	MouseClick LEFT, 52, 98
;;	Sleep, 500
;;	MouseClick LEFT, 52, 58
;;	Return
;;;;; ------------------------------------------------
;;F7::
;; 	MouseClick LEFT, 52, 58
;; 	Return
;;;;; ------------------------------------------------
;;F8::
;; 	MouseClick LEFT, 52, 331
;; 	Return
;;;;;;; ------------------------------------------------
;;F7::
;; 	Loop, 3
;; 	{
;; 		MouseClick LEFT, 52, 58
;; 		Sleep, 1500
;; 		MouseClick LEFT, 52, 331
;; 		Sleep, 1500
;; 	}
;;	MouseClick LEFT, 52, 58
;;	Return 
;;;; ------------------------------------------------
;;
;;; ミスミカタログ
; ------------------------------------------------
F7::
 	MouseClick LEFT, 28, 658
 	Return
;;; ------------------------------------------------
F8::
 	MouseClick LEFT, 1568, 658
 	Return
;;; ------------------------------------------------
;; Alt + Space
!Space::Send, {vkF3sc029}

;;-----
;;パテントスクエアの抄録をダウンロードして、URLをハイパーリンクにしたかった件
;;なお、エクセルのキー操作は、F2エンターでハイパーリンクになる
;;-----
;;a::
;;Loop, 145
;;{
;;    Send, {F2}
;;    Send, {ENTER}
;;}
;;Return
;;      
